#!/usr/bin/env perl

use Test2::V0;

# NB: It is best to specify the number of tests where possible.
plan(2);


use Catalyst::Test 'Demo::PetStore';


my $response = _request('/');
ok( $response->is_success, 'Request should succeed' );
is($response->code, '200', 'Got expected HTTP status');


sub _request {
    my $response = request(@_);

    note("REQUEST:\n" . $response->request->as_string);
    note("RESPONSE:\n" . $response->as_string);

    return $response;
}

