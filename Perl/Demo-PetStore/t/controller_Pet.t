
use Test2::V0;

# NB: It is best to specify the number of tests where possible.
plan(9);


use Catalyst::Test 'Demo::PetStore';
use HTTP::Headers::ActionPack;
use HTTP::Headers::ActionPack::MediaType;
use HTTP::Headers::ActionPack::MediaTypeList;
use HTTP::Request::Common qw( DELETE GET POST PUT );
use JSON;
use XML::Simple;

my $list_uri                         = '/pet';
my $by_id_test_name_uri              = $list_uri . '/{petId}';
my $by_id_upload_image_test_name_uri = $list_uri . '/{petId}/uploadImage';
my $by_id_uri                        = $list_uri . '/%d';
my $find_by_status_uri               = $list_uri . '/findByStatus';
my $find_by_tags_uri                 = $list_uri . '/findByTags';
my $by_id_upload_image_uri           = $by_id_uri . '/uploadImage';

my $json_encoder = JSON->new;
if ( $ENV{DEBUG} ) {
    $json_encoder->pretty;
}

my $form_type = HTTP::Headers::ActionPack::MediaType->new(
        'application/x-www-form-urlencoded');
my $json_type = HTTP::Headers::ActionPack::MediaType->new('application/json');
my $xml_type  = HTTP::Headers::ActionPack::MediaType->new('application/xml');


my $updated_pet = {
    id        => 1,
    name      => 'Felix',
    photoUrls => [],
    status    => 'pending',

    category  => {
        id   => 1,
        name => 'cats',
    },

    tags      => [
        {
            id   => 1,
            name => 'has-stripes',
        },
        {
            id   => 5,
            name => 'is-active',
        },
    ]
};
my $updated_pet_json = $json_encoder->encode($updated_pet);


subtest 'GET ' . $list_uri => sub {
    plan(1);

    my $request = GET $list_uri;
    my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
        HTTP::Headers::ActionPack::MediaType->new(
            '*/*' => (
                q => '0.9',
            ),
        ),
    );
    $request->headers->header('Accept' => $accept);

    my $response = _request($request);
    is($response->code, '405', 'Got expected HTTP status');

};


subtest 'GET ' . $find_by_status_uri => sub {
    my @types = ( $json_type, $xml_type );

    plan(scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $request_type = $types[$i];

        subtest 'success response Accept: ' . $request_type => sub {
            plan(2);

            my $request = GET $find_by_status_uri;
            $request->headers->header('Accept' => $request_type);

            my $response = _request($request);
            is($response->code, '200', 'Got expected HTTP status');

            my $content_type = $response->headers->header('Content-Type');
            ok($content_type->match($request_type), 'Got expected content type header')
                    or do {
                        diag('got      : ' . $content_type);
                        diag('expected : ' . $request_type);
                    };

        };
    }
};


subtest 'GET ' . $find_by_tags_uri => sub {
    my @types = ( $json_type, $xml_type );

    plan(scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $request_type = $types[$i];

        subtest 'success response Accept: ' . $request_type => sub {
            plan(2);

            my $request = GET $find_by_tags_uri;
            $request->headers->header('Accept' => $request_type);

            my $response = _request($request);
            is($response->code, '200', 'Got expected HTTP status');

            my $content_type = $response->headers->header('Content-Type');
            ok($content_type->match($request_type), 'Got expected content type header')
                    or do {
                        diag('got      : ' . $content_type);
                        diag('expected : ' . $request_type);
                    };
        };
    }
};


subtest 'POST ' . $list_uri => sub {
    my $new_pet = {
        name      => 'Crackers',
        photoUrls => [],
        status    => 'available',

        category  => {
            id   => 3,
            name => 'birds',
        },

        tags      => [
            {
                id   => 3,
                name => 'parrot',
            },
            {
                id   => 4,
                name => 'can-talk',
            },
        ]

    };
    my $new_pet2 = { %{ $new_pet } };
    $new_pet2->{name} = 'Polly';
    my $new_pet3 = { %{ $new_pet } };
    $new_pet3->{name} = 'Mr. Feathers';

    my @types    = ( $form_type, $json_type, $xml_type );
    my @requests = (
        POST($list_uri,
            [
                # NB: garbage data at this point
                foo => 'bar',
            ],
        ),
        POST($list_uri,
            Accept         => $json_type,
            'Content-Type' => $json_type,
            Content        => $json_encoder->encode($new_pet2),
        ),
        POST($list_uri,
            Accept         => $xml_type,
            'Content-Type' => $xml_type,
            Content        => XML::Simple->new->XMLout($new_pet3),
        ),
    );

    # NB: POST contstructor for form data is fussy.  Set header
    # seperately.
    # NB: Want JSON data back from this request.
    $requests[0]->header(Accept => $json_type);

    plan(scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $post_type = $types[$i];
        my $request   = $requests[$i];

        subtest 'Content-Type: ' . $post_type => sub {
            plan(2);

            my $response = _request($request);
            is($response->code, '201', 'Got expected HTTP status');

            my $location = $response->headers->header('Location');
            my $uri_pet_by_id_regex = $list_uri . '/\d+$';
            like($location, qr!$uri_pet_by_id_regex!, , 'Got expected locatiion header')
        };
    }

};


subtest 'GET ' . $by_id_test_name_uri => sub {
    my @types = ( $json_type, $xml_type );

    plan(1 + scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $request_type = $types[$i];

        subtest 'success response Accept: ' . $request_type => sub {
            plan(2);

            my $id = 1;
            my $request = GET sprintf($by_id_uri, $id);
            $request->headers->header('Accept' => $request_type);

            my $response = _request($request);
            is($response->code, '200', 'Got expected HTTP status');

            my $content_type = $response->headers->header('Content-Type');
            ok($content_type->match($request_type), 'Got expected content type header')
                    or do {
                        diag('got      : ' . $content_type);
                        diag('expected : ' . $request_type);
                    };
        };
    }


    subtest 'missing response' => sub {
        plan(1);

        my $id = 0;
        my $request = GET sprintf($by_id_uri, $id);
        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '404', 'Got expected HTTP status');
    };

};


subtest 'POST ' . $by_id_test_name_uri => sub {
    my $id = 1;

    my @types = ( $json_type, $xml_type );
    my @requests = (
        POST(sprintf($by_id_uri, $id),
            'Content-Type' => $json_type,
            Content        => $json_encoder->encode($updated_pet),
        ),
        POST(sprintf($by_id_uri, $id),
            'Content-Type' => $xml_type,
            Content        => XML::Simple->new->XMLout($updated_pet),
        ),
    );

    plan(1 + scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $request_type = $types[$i];
        my $request      = $requests[$i];

        subtest 'success response Content-Type: ' . $request_type => sub {
            plan(2);

            my $response = _request($request);
            is($response->code, '204', 'Got expected HTTP status');

            my $content_type = $response->headers->header('Content-Type') // '';
            is('' . $content_type, '', 'Got no content type header');

        };
    }


    subtest 'missing response' => sub {
        plan(2);

        my $id = 0;
        my $request = POST sprintf($by_id_uri, $id),
            'Content-Type' => $json_type,
            Content        => $updated_pet_json;

        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '404', 'Got expected HTTP status');

        my $content_type = $response->headers->header('Content-Type');
        is($content_type, $json_type, 'Got expected content type header');
    };

};


subtest 'PUT ' . $list_uri => sub {
    my @types = ( $json_type, $xml_type );
    my @requests = (
        PUT($list_uri,
            Accept         => $json_type,
            'Content-Type' => $json_type,
            Content        => $updated_pet_json,
        ),
        PUT($list_uri,
            Accept         => $xml_type,
            'Content-Type' => $xml_type,
            Content        => XML::Simple->new->XMLout($updated_pet),
        ),
    );

    plan(1 + scalar @types);

    for ( my $i = 0; $i < @types; $i++ ) {
        my $request_type = $types[$i];
        my $request      = $requests[$i];

        subtest 'success response Content-Type: ' . $request_type => sub {
            plan(2);

            my $response = _request($request);
            is($response->code, '204', 'Got expected HTTP status');

            my $content_type = $response->headers->header('Content-Type') // '';
            is('' . $content_type, '', 'Got no content type header');

        };
    }


    subtest 'missing response' => sub {
        plan(1);

        my $request = PUT $list_uri;
        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '404', 'Got expected HTTP status');
    };

};


subtest 'DELETE ' . $by_id_test_name_uri => sub {
    plan(2);


    subtest 'success response' => sub {
        plan(2);

        my $id = 2;
        my $request = DELETE sprintf($by_id_uri, $id);
        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '204', 'Got expected HTTP status');

        my $content_type = $response->headers->header('Content-Type') // '';
        is('' . $content_type, '', 'Got no content type header');

    };


    subtest 'missing response' => sub {
        plan(1);

        my $id = 0;
        my $request = DELETE sprintf($by_id_uri, $id);
        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '404', 'Got expected HTTP status');
    };

};


subtest 'POST ' . $by_id_upload_image_test_name_uri => sub {
    plan(2);


    subtest 'success response' => sub {
        plan(2);

        my $id = 1;
        my $request = POST sprintf($by_id_upload_image_uri, $id),
            # NB: just posting *something* at the moment
            'Content-Type' => $json_type,
            Content        => $updated_pet_json;

        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '204', 'Got expected HTTP status');

        my $content_type = $response->headers->header('Content-Type') // '';
        is('' . $content_type, '', 'Got no content type header');

    };


    subtest 'missing response' => sub {
        plan(1);

        my $id = 0;
        my $request = POST sprintf($by_id_upload_image_uri, $id);
        my $accept = HTTP::Headers::ActionPack::MediaTypeList->new(
            HTTP::Headers::ActionPack::MediaType->new(
                $json_type->type => (
                    q => '0.9',
                ),
            ),
        );
        $request->headers->header('Accept' => $accept);

        my $response = _request($request);
        is($response->code, '404', 'Got expected HTTP status');
    };

};


sub _request {
    my $response = request(@_);

    # NB: do this before logging to get a more consistent log output
    my $pack = HTTP::Headers::ActionPack->new;
    $pack->inflate($response->headers);

    note("REQUEST:\n" . $response->request->as_string);
    note("RESPONSE:\n" . $response->as_string);

    return $response;
}

