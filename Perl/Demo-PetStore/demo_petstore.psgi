use strict;
use warnings;

use Demo::PetStore;

my $app = Demo::PetStore->apply_default_middlewares(Demo::PetStore->psgi_app);
$app;

