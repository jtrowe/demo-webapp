package Demo::PetStore::Action::Deserialize;

# ABSTRACT: Deserializes request if and only if the request contains content.

use Moose;
use namespace::autoclean;

extends 'Catalyst::Action::Deserialize';

=head1 METHODS

=cut

=head2 execute

Checks Content-Length before attempting to deserialize request content.

=cut

sub execute {
    my $self = shift;
    my ( $controller, $c ) = @_;

    my $content_length = $c->req->header('Content-Length') // 0;
    unless ( $content_length > 0 ) {
        return 1;
    }

    $self->maybe::next::method(@_);

    return 1;
} # execute

1;
