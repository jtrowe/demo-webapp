package Demo::PetStore::Controller::Pet;

# ABSTRACT: Controller handling Pet endpoints

use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller::REST'; }

use Data::Dump qw( pp );


__PACKAGE__->config(
    compliance_mode => 1,
    deserialize_map => {
        'application/json'                  => 'JSON',
        'application/xml'                   => 'XML::Simple',
    },
    map             => {
        'application/json' => 'JSON',
        'application/xml'  => 'XML::Simple',
    },
);


# a fake model for development of API endpoints
my %pets = map { $_->{id} => $_ } (
    {
        id        => 1,
        name      => 'Felix',
        photoUrls => [],
        status    => 'available',

        category  => {
            id   => 1,
            name => 'cats',
        },

        tags      => [
            {
                id   => 1,
                name => 'has-stripes',
            },
        ]

    },
    {
        id        => 2,
        name      => 'Rover',
        photoUrls => [],
        status    => 'available',

        category  => {
            id   => 2,
            name => 'dogs',
        },

        tags      => [
            {
                id   => 2,
                name => 'floppy-ears',
            },
            {
                id   => 3,
                name => 'spots',
            },
        ]

    },
);

=head1 METHODS

=cut

=head2 find_by_status

Handler for getting a list of pets filtered by status.

=cut

sub find_by_status :Path('findByStatus') :Args(0) :ActionClass('REST') {}


=head2 find_by_status_GET

Get a list of pets filtered by status.

=cut

sub find_by_status_GET {
    my ( $self, $c ) = @_;

    $self->status_ok(
        $c,
        entity => [ values %pets ],
    );
} # find_by_status_GET


=head2 find_by_tags

Handler for getting a list of pets filtered by tags.

=cut

sub find_by_tags :Path('findByTags') :Args(0) :ActionClass('REST') {}


=head2 find_by_tags_GET

Get a list of pets filtered by tags.

=cut

sub find_by_tags_GET {
    my ( $self, $c ) = @_;

    $self->status_ok(
        $c,
        entity => [ values %pets ],
    );
} # find_by_tags_GET


=head2 pet_by_id_base

Base handler for all methods working with a single Pet object.

=cut

sub pet_by_id_base :PathPart(pet) :Chained(/) :CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;

    my $pet = $pets{$id};
    unless ( $pet ) {
        $self->status_not_found(
            $c,
            message => 'Resource not found',
        );
        $c->detach;
    }

    $c->stash->{pet_id} = $id;
    $c->stash->{pet}    = $pets{$id};

} # pet_by_id_base


=head2 pet_by_id

Base REST handler for all methods working with a single Pet object.

=cut

sub pet_by_id :Chained(pet_by_id_base) :PathPart('') :Args(0) :ActionClass('REST') {}


=head2 pet_by_id_DELETE

DELETE handler for pet_by_id

=cut

sub pet_by_id_DELETE {
    my ( $self, $c ) = @_;

    my $id = $c->stash->{pet_id};

    delete $pets{$id};

    $self->status_no_content($c);
} # pet_by_id_DELETE


=head2 pet_by_id_GET

GET handler for pet_by_id

=cut

sub pet_by_id_GET {
    my ( $self, $c ) = @_;

    my $pet = $c->stash->{pet};

    $self->status_ok(
        $c,
        entity => $pet,
    );
} # pet_by_id_GET


=head2 pet_by_id_POST

POST handler for pet_by_id

=cut

sub pet_by_id_POST {
    my ( $self, $c, $id ) = @_;

    my $pet = $c->stash->{pet};
    $pets{$pet->{id}} = $pet;

    $self->status_no_content($c);
} # pet_by_id_POST


=head2 pet_by_id_upload_image

Base REST handler for all methods working with a single Pet object.

=cut

sub pet_by_id_upload_image :Chained(pet_by_id_base) :PathPart('uploadImage') :Args(0) :ActionClass('REST') {}


=head2 pet_by_id_upload_image_POST

POST handler for pet_by_id_upload_image

=cut

sub pet_by_id_upload_image_POST {
    my ( $self, $c ) = @_;

    my $id = $c->stash->{pet_id};

    $self->status_no_content($c);
} # pet_by_id_upload_image_POST


=head2 list_pets

Returns a list of Pets.

=cut

sub list_pets :Path :Args(0) :ActionClass('REST') {}


=head2 list_pets_POST

POST handler for list_pets.

=cut

sub list_pets_POST {
    my ( $self, $c ) = @_;

    my $data = $c->req->data;
    $c->log->debug("data:\n" . pp($data));

    my @keys = sort keys %pets;
    my $id = $keys[-1] + 1;
    $data->{id} = $id;
    $pets{$id} = $data;

    my $location = $c->uri_for($self->action_for('pet_by_id'), [ $id ]);

    $self->status_created(
        $c,
        location => $location,
        entity   => $data,
    );
} # list_pets_POST


=head2 list_pets_PUT

PUT handler for list_pets

=cut

sub list_pets_PUT {
    my ( $self, $c ) = @_;

    my $data = $c->req->data;
    $c->log->debug("data:\n" . pp($data));

    my $id = $data->{id};

    # FIXME Would like to use a chain to DRY, but going to leave that for later.
    my $pet = $pets{$id};
    unless ( $pet ) {
        $self->status_not_found(
            $c,
            message => 'Resource not found',
        );
        $c->detach;
    }

    $pets{$id} = $data;

    $self->status_no_content($c);
} # list_pets_PUT


=head2 begin

Deserializes request content when present.

=cut

sub begin :ActionClass('+Demo::PetStore::Action::Deserialize') {}


=head2 end

Serializes response content when present.

=cut

sub end :ActionClass('Serialize') {}


=encoding utf8

=cut

__PACKAGE__->meta->make_immutable;

1;
