// 2021.08.5

Add GET pets/{petId} endpoint.

Add DELETE pets/{petId} endpoint.

Add PUT pets/{petId} endpoint.

I realized mis-implemented the POST and PUT endoint according to the spec.
POST takes a {petId}, PUT does not.

Rename endpoints /pets to /pet. (Which is my preference anyways).

Change PUT /pet/{petId} endpoint to PUT /pet.

Add POST /pet/{petId} endpoint.

Add GET /pet/findByStatus endpoint.
Remove GET /pet endpoint; it is not in the spec.

Add GET /pet/findByTags endpoint.

Add POST /pet/{petId}/uploadImage endpoint.

**All** endpoints for `/pet` from the spec have an endpoint.
Most are preforming the simplest correct-ish response possible.


